﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public GameObject bullet;
    public GameObject explosion;
    public float shotDelay = 2.0f;
    //移動量
    public float enemyMoveX = -0.03f;
    public float enemyMoveY = -0.03f;
    //エネミーのHP
    public int HP = 1;
    // スコアのポイント
    public int point = 100;
    private Manager manager;

    // Use this for initialization
    IEnumerator Start()
    {
        // Managerコンポーネントをシーン内から探して取得する
        manager = FindObjectOfType<Manager>();

        while (true)
        {
            for (int i = 0; i < transform.childCount; i++)
            {

                if (manager.IsPlaying() == true)
                {
                    
                    Transform shotPosition = transform.GetChild(i);

                    Instantiate(bullet, shotPosition.position, shotPosition.rotation);
                }
            }

            // shotDelay秒待つ
            yield return new WaitForSeconds(shotDelay);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Managerコンポーネントをシーン内から探して取得する
        manager = FindObjectOfType<Manager>();

        objectScreenOut();

        if (manager.IsPlaying() == true)
        {
            transform.Translate(enemyMoveX, enemyMoveY, 0);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //弾に当たったら
    void OnTriggerEnter2D(Collider2D c)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        if(layerName == "PlayerBullet")
        {
            HP--;
            Explosion();

            if (HP <= 0)
            {
                // スコアコンポーネントを取得してポイントを追加
                FindObjectOfType<Score>().AddPoint(point);
                Destroy(gameObject);
            }
        }

    }

    //オブジェクトが画面外に出たときの処理
    void objectScreenOut()
    {
        //オブジェクトの位置取得
        Vector3 posObj = Camera.main.WorldToViewportPoint(transform.position);

        //オブジェクトが画面外かどうか
        if (posObj.x > 1.5f ||
            posObj.x < -0.5f ||
            posObj.y < 0)
        {
            Destroy(gameObject);
        }

    }

    //爆発
    public void Explosion()
    {
        Instantiate(explosion, transform.position, transform.rotation);
    }


}