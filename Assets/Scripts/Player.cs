﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private Vector3 pos;
    public GameObject bullet;


    // Use this for initialization
    IEnumerator Start()
    {
        while (true)
        {
            // ショット音を鳴らす
            GetComponent<AudioSource>().Play();

            Instantiate(bullet, transform.position, transform.rotation);
            yield return new WaitForSeconds(0.05f);
        }
       
    }

    // Update is called once per frame
    void OnGUI()
    {
        if (Event.current.type == EventType.MouseDrag)
        {
            pos = Input.mousePosition;
        }
         transform.position = Camera.main.ScreenToWorldPoint(pos);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        if (layerName == "EnemyBullet")
        {
            // Managerコンポーネントをシーン内から探して取得し、GameOverメソッドを呼び出す
            FindObjectOfType<Manager>().GameOver();

            Destroy(gameObject);
        }

        if (layerName == "Enemy")
        {
            // Managerコンポーネントをシーン内から探して取得し、GameOverメソッドを呼び出す
            FindObjectOfType<Manager>().GameOver();

            Destroy(gameObject);
        }

    }

}