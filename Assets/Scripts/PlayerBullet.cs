﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

    public int speed = 10;

    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.up.normalized * speed;
    }

    // Update is called once per frame
    void Update()
    {
        objectScreenOut();
    }

    //オブジェクトが画面外に出たときの処理
    void objectScreenOut()
    {
        //オブジェクトの位置取得
        Vector3 posObj = Camera.main.WorldToViewportPoint(transform.position);

        //オブジェクトが画面外かどうか
        if (posObj.x > 1 ||
            posObj.x < 0 ||
            posObj.y > 0.97 ||
            posObj.y < 0)
        {
            Destroy(gameObject);
        }

    }
}