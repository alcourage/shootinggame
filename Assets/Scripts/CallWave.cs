﻿using UnityEngine;
using System.Collections;

public class CallWave : MonoBehaviour {

    public GameObject[] waves;

    private int nowWave;

    private Manager manager;

    // Use this for initialization
    IEnumerator Start()
    {

        if (waves.Length == 0)
        {
            yield break;
        }

        // Managerコンポーネントをシーン内から探して取得する
        manager = FindObjectOfType<Manager>();

        while (true)
        {
            // タイトル表示中は待機
            while (manager.IsPlaying() == false)
            {
                nowWave = 0;
                yield return new WaitForEndOfFrame();
            }

            //Waveの作成
            GameObject wave = (GameObject)Instantiate(waves[nowWave], transform.position, Quaternion.identity);

            //WaveをCallWaveの子要素にする
            wave.transform.parent = transform;

            // Waveの子要素のEnemyが全て削除されるまで待機する
            while (wave.transform.childCount != 0)
            {
                yield return new WaitForEndOfFrame();
            }


            // Waveの削除
            Destroy(wave);

            // 格納されているWaveを全て実行したらcurrentWaveを0にする（最初から -> ループ）
            if (waves.Length <= ++nowWave)
            {
                nowWave = 0;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
